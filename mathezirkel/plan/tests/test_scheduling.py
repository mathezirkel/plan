#  Plan is a tool to create time tables for the Mathecamp Augsburg.
#  Copyright (c) 2020-2020 Matheschülerzirkel Augsburg
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import List, Dict

import pytest

from mathezirkel.plan.schedule import create_time_table


def test_small_example():
    # Given
    tutors: List[str] = ["Anna", "Sven", "Ingo", "Dominik"]
    sessions: List[str] = ["Sunday_First", "Sunday_Second"]
    outages: Dict[str, List[str]] = {
        "Dominik": ["Sunday_First"]
    }
    preferences: Dict[str, Dict[int, int]] = {
        "Anna": {5: 1},
        "Ingo": {5: -1, 6: 1}
    }
    circles: Dict[str, List[int]] = {
        "5a": [5],
        "5b": [5],
        "6": [6]
    }

    # When
    result: Dict[str, Dict[str, str]] = create_time_table(sessions=sessions, circles=circles, tutors=tutors,
                                                          outages=outages, preferences=preferences)
    # Then
    # Every circle has a tutor assigned at every session
    for session in sessions:
        for circle in circles.keys():
            assert result[session][circle] in tutors

    # Ever tutor is scheduled at least once
    for tutor in tutors:
        assert len([1 for session in sessions for circle in circles.keys() if result[session][circle] == tutor]) >= 1

    # Outages are observed
    for tutor_with_outage, tutor_outages in outages.items():
        for outage in tutor_outages:
            for circle in circles:
                assert result[outage][circle] != tutor_with_outage


def test_medium_example():
    # Given
    tutors: List[str] = ["Anna", "Sven", "Ingo", "Dominik", "Meru", "Felix", "Kathrin", "Leonie", "Georg", "Matthias",
                         "Andreas", "Carola"]
    sessions: List[str] = ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second", "Tuesday_First",
                           "Tuesday_Second"]
    outages: Dict[str, List[str]] = {
        "Dominik": ["Sunday_First", "Sunday_Second"],
        "Meru": ["Tuesday_Second"],
        "Carola": ["Tuesday_First", "Tuesday_Second"]
    }
    preferences: Dict[str, Dict[int, int]] = {
        "Anna": {5: 1},
        "Ingo": {5: -1, 6: 1},
        "Meru": {7: 2},
        "Leonie": {9: -2}
    }
    circles: Dict[str, List[int]] = {
        "5a": [5],
        "5b": [5],
        "6": [6],
        "7a": [7],
        "7b": [7],
        "7c": [7],
        "8a": [8],
        "8b": [8],
        "9": [9]
    }

    # When
    result: Dict[str, Dict[str, str]] = create_time_table(sessions=sessions, circles=circles, tutors=tutors,
                                                          outages=outages, preferences=preferences)
    print(result)
    # Then
    # Every circle has a tutor assigned at every session
    for session in sessions:
        for circle in circles.keys():
            assert result[session][circle] in tutors

    # Ever tutor is scheduled at least once
    for tutor in tutors:
        assert len([1 for session in sessions for circle in circles.keys() if result[session][circle] == tutor]) >= 1

    # Outages are observed
    for tutor_with_outage, tutor_outages in outages.items():
        for outage in tutor_outages:
            for circle in circles:
                assert result[outage][circle] != tutor_with_outage


def test_large_example():
    # Given
    tutors: List[str] = ["Meru",
                         "Samuel1",
                         "Ingo",
                         "Andreas",
                         "Xaver",
                         "Benedikt",
                         "Georg",
                         "Jonas",
                         "Samuel2",
                         "Alexander",
                         "Julius",
                         "Sven",
                         "Kilian",
                         "Matthias",
                         "Dominik",
                         "Felix",
                         "Michael",
                         "David",
                         "Tanja",
                         "Caterina",
                         "Anja",
                         "Leonie",
                         "Ninwe",
                         "Veronika",
                         "Jana",
                         "Anna",
                         "Carola",
                         "Louisa"]

    circles: Dict[str, List[int]] = {"5a": [5], "5b": [5], "5c": [5],
                                     "6a": [6], "6b": [6], "6c": [6], "6d": [6],
                                     "7a": [7], "7b": [7], "7c": [7],
                                     "8a": [8], "8b": [8],
                                     "9a": [9], "9b": [9], "9c": [9],
                                     "10": [10],
                                     "11/12a": [11, 12], "11/12b": [11, 12]}

    sessions: List[str] = ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second", "Tuesday_First",
                           "Tuesday_Second", "Thursday_First", "Thursday_Second", "Friday_First", "Friday_Second",
                           "Saturday_First", "Saturday_Second"]

    outages: Dict[str, List[str]] = {"Samuel1": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                                 "Tuesday_First", "Tuesday_Second"],
                                     "Xaver": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                               "Saturday_Second"],
                                     "Benedikt": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                                  "Saturday_Second"],
                                     "Jonas": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                               "Saturday_Second"],
                                     "Julius": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                                "Saturday_Second"],
                                     "Matthias": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                                  "Tuesday_First",
                                                  "Tuesday_Second"],
                                     "Michael": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                                 "Tuesday_First",
                                                 "Tuesday_Second"],
                                     "Caterina": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                                  "Tuesday_First",
                                                  "Tuesday_Second"],
                                     "Anja": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                              "Saturday_Second"],
                                     "Ninwe": ["Thursday_Second", "Friday_First", "Friday_Second", "Saturday_First",
                                               "Saturday_Second"],
                                     "Veronika": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                                  "Tuesday_First",
                                                  "Tuesday_Second"],
                                     "Jana": ["Sunday_First", "Sunday_Second", "Monday_First", "Monday_Second",
                                              "Tuesday_First"],
                                     "Louisa": ["Sunday_Second"]}

    preferences: Dict[str, Dict[int, int]] = {"Samuel1": {11: 1, 12: 1},
                                              "Ingo": {11: 1, 12: 1},
                                              "Andreas": {8: 1, 9: 1, 10: 1, 11: 1, 12: 1},
                                              "Xaver": {11: 1, 12: 1},
                                              "Jonas": {8: 1, 9: 1, 10: 1, 11: 1, 12: 1},
                                              "Samuel2": {11: 1, 12: 1},
                                              "Alexander": {6: 1, 9: 1},
                                              "Julius": {8: 1, 9: 1, 10: 1, 11: 1, 12: 1},
                                              "Kilian": {8: 1, 9: 1, 10: 1, 11: 1, 12: 1},
                                              "Matthias": {7: 1, 8: 1},
                                              "Dominik": {5: 1, 6: 1, 7: 1, 8: 1, 9: 1},
                                              "Felix": {11: 1, 12: 1},
                                              "David": {8: 1, 9: 1, 10: 1, 11: 1, 12: 1},
                                              "Tanja": {5: 1, 6: 1, 7: 1, 8: 1, 9: 1},
                                              "Caterina": {5: 1, 6: 1, 7: 1, 8: 1, 9: 1},
                                              "Anja": {5: 1, 6: 1, 7: 1},
                                              "Leonie": {5: 2, 6: 2, 9: -5, 10: -5, 11: -5, 12: -5},
                                              "Ninwe": {5: 1, 6: 1, 7: 1, 8: 1},
                                              "Veronika": {5: 1, 6: 1, 7: 1, 8: 1},
                                              "Jana": {5: 1, 6: 1, 7: 1},
                                              "Louisa": {5: 1, 6: 1, 7: 1, 8: 1}}

    # When
    result: Dict[str, Dict[str, str]] = create_time_table(sessions=sessions, circles=circles, tutors=tutors,
                                                          outages=outages, preferences=preferences)
    print(result)
    # Then
    # Every circle has a tutor assigned at every session
    for session in sessions:
        for circle in circles.keys():
            assert result[session][circle] in tutors

    # Ever tutor is scheduled at least once
    for tutor in tutors:
        assert len([1 for session in sessions for circle in circles.keys() if result[session][circle] == tutor]) >= 1

    # Outages are observed
    for tutor_with_outage, tutor_outages in outages.items():
        for outage in tutor_outages:
            for circle in circles:
                assert result[outage][circle] != tutor_with_outage
