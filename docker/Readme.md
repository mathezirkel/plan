# Docker file for running plan tests

This directory contains files for setting up a Docker image that can
be used for running tests in a CI environment. In particular it installs
SCIP on top of a Python distribution.

Notice that SCIP is distributed under the [ZIB Academic licence](LICENCE)
meaning that we should not distribute the SCIP binaries. Therefore, please
do not check the `deb` file of SCIP into this repository or push the
Docker image to any public registry.

## How to build
In order to build the image, just download the correct version of SCIP
for Debian from https://scip.zib.de/index.php#download. ALso make sure
that docker is installed on your system and accessible by your user.

Then execute
```commandline
$ ./build.sh
```

## How to publish to the private registry on Gitlab
After logging in to the registry at https://gitlab.com/mathezirkel/plan/container_registry/
via
```commandline
$ docker login registry.gitlab.com 
```
you can push the image to Gitlab by executing e.g.
```commandline
$ docker push registry.gitlab.com/mathezirkel/plan/python36_scip:7.0.0
```

## How to update
In order to update either python or SCIP, you need to modify the versions in
 * [Dockerfile](Dockerfile) and
 * [build.sh](build.sh).
